<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE policyconfig PUBLIC
"-//freedesktop//DTD PolicyKit Policy Configuration 1.0//EN"
"http://www.freedesktop.org/standards/PolicyKit/1/policyconfig.dtd">
<policyconfig>
	<!--
		(c) 2015 Christoph Grenz <christophg@grenz-bonn.de>
		    This file is part of rootforce.

		Rootforce is free software: you can redistribute it and/or modify
		it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 2 of the License, or
		(at your option) any later version.

		Rootforce is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with Rootforce.  If not, see <http://www.gnu.org/licenses/>.
	-->

	<vendor>Christoph Grenz</vendor>
	<vendor_url>https://gitlab.com/rootforce/rootforce</vendor_url>

	<action id="de.grenz-bonn.rootforce">
		<description>Raise a processes permissions with `rootforce'</description>
		<description xml:lang="de">Erhöhen der Rechte eines Prozesses mit `rootforce'</description>
		<message>Authentication is required to raise a processes permissions ($(command_line))</message>
		<message xml:lang="de">Authorisierung wird benötigt um die Rechte eines Prozesses zu erhöhen ($(command_line))</message>
		<icon_name>task-delegate</icon_name>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>no</allow_inactive>
			<allow_active>auth_admin</allow_active>
		</defaults>
		<annotate key="org.freedesktop.policykit.exec.path">$SBINDIR/do_rootforce</annotate>
	</action>
	
	<action id="de.grenz-bonn.rootforce.undo">
		<description>Lower processes permissions raised with `rootforce'</description>
		<description xml:lang="de">Senken der Rechte eines Prozesses, welche mit mit `rootforce' erhöht wurden</description>
		<message>Authentication is required to restore a processes permissions which were raised with "rootforce"</message>
		<message xml:lang="de">Authorisierung wird benötigt um die Rechte eines Prozesses, welche mit "rootforce" erhöht wurden, wiederherzustellen</message>
		<icon_name>task-ongoing</icon_name>
		<defaults>
			<allow_any>no</allow_any>
			<allow_inactive>auth_admin</allow_inactive>
			<allow_active>yes</allow_active>
		</defaults>
		<annotate key="org.freedesktop.policykit.exec.path">$SBINDIR/undo_rootforce</annotate>
	</action>

</policyconfig>
