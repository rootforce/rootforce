#!/usr/bin/python
# encoding: utf-8

'''
	(c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
	This file is part of rootforce.

	Rootforce is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

	Rootforce is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Rootforce.  If not, see <http://www.gnu.org/licenses/>.
'''

import os, sys
from os.path import join as pjoin
from subprocess import Popen, PIPE
import dbus
from xml.dom import minidom

SBINDIR='./'

DO_ROOTFORCE=pjoin(SBINDIR, 'do_rootforce')
UNDO_ROOTFORCE=pjoin(SBINDIR, 'undo_rootforce')

def get_proc_name(pid):
	with open('/proc/'+str(pid)+'/stat', 'r') as f:
		return f.readline().split()[1][1:-1]

def get_x_foreground_window():
	return Popen(['xprop', '-notype', '-root', '_NET_ACTIVE_WINDOW'], stdout=PIPE).communicate()[0].strip().split()[4]

def get_x_foreground_pid(winid=None):
	try:
		if winid is None:
			winid = Popen(['xprop', '-notype', '-root', '_NET_ACTIVE_WINDOW'], stdout=PIPE).communicate()[0].strip().split()[4]
		return int(Popen(['xprop', '-notype', '-id', str(winid), '_NET_WM_PID'], stdout=PIPE).communicate()[0].strip().split()[2])
	except ValueError:
		return None
	except IndexError:
		return None

def get_terminal_foreground_pid():
	selfpid = os.getpid()
	termid = Popen(['fgconsole'], stdout=PIPE).communicate()[0].strip()
	if termid.isdigit():
		termdev = '/dev/tty'+termid
		stat = os.stat(termdev)
		termid = str(stat.st_rdev)
		
		result = []
		for item in os.listdir('/proc'):
			if item.isdigit():
				with open('/proc/'+item+'/stat', 'rb') as f:
					data = f.readline().split()
					if termid == data[6]:
						if data[5] == data[3]:
							result.append(int(item))
		return max((get_pid_starttime(pid), pid) for pid in result if pid != selfpid)[1]
	return None

def get_child_pids(pid):
	result = []
	for item in os.listdir('/proc'):
		if item.isdigit():
			with open('/proc/'+item+'/stat', 'rb') as f:
				ppid = int(f.readline().split()[3])
				if ppid == pid:
					result.append(int(item))
	return result

def get_pid_starttime(pid):
	with open('/proc/'+str(pid)+'/sched', 'rb') as f:
		while True:
			line = f.readline()
			if line.startswith(b'se.exec_start '):
				return float(line.strip().split()[2])


def get_descendant_pids(pid, include_self=True):
	result = []
	if include_self:
		result.append(pid)
	pids = get_child_pids(pid)
	for cpid in pids:
		result.append(cpid)
		result.extend(get_descendant_pids(cpid, False))
	return result

def get_display_type():
	global _display_type
	if not _display_type:
		termid = Popen(['fgconsole'], stdout=PIPE).communicate()[0].strip()
		if termid and termid.isdigit() and int(termid) < 7:
			_display_type = 'terminal'
		else:
			_display_type = 'X'
	return _display_type
_display_type = None

if os.path.exists('/usr/bin/pkexec'):
	def do_raise_perms(pid):
		return Popen(['pkexec', DO_ROOTFORCE, str(pid), get_proc_name(pid)]).wait() == 0
	def do_lower_perms():
		return Popen(['pkexec', UNDO_ROOTFORCE]).wait() == 0
elif get_display_type() == 'X':
	if 'KDE_FULL_SESSION' in os.environ:
		def do_raise_perms(pid):
			text = 'Rooting {0} [{1}]'.format(get_proc_name(pid), int(pid))
			return Popen(['kdesudo', '--comment', text, DO_ROOTFORCE, str(pid), get_proc_name(pid)]).wait() == 0
		def do_lower_perms():
			return Popen(['kdesudo', '--comment', 'Restore permissions', UNDO_ROOTFORCE]).wait() == 0
	else:
		def do_raise_perms(pid):
			text = 'Rooting {0} [{1}]'.format(get_proc_name(pid), int(pid))
			return Popen(['gksudo', '--description', text, DO_ROOTFORCE, str(pid), get_proc_name(pid)]).wait() == 0
		def do_lower_perms():
			return Popen(['gksudo', '--description', 'Restore permissions', UNDO_ROOTFORCE]).wait() == 0
else:	
	def do_raise_perms(pid):
		print('Rooting {0} [{1}]'.format(get_proc_name(pid), int(pid)))
		return Popen(['sudo', DO_ROOTFORCE, str(pid), get_proc_name(pid)]).wait() == 0
	def do_lower_perms():
		print("Restore permissions")
		return Popen(['sudo', UNDO_ROOTFORCE]).wait() == 0

def raise_perms(pid):
	if do_raise_perms(pid):
		try:
			session = dbus.SessionBus()
			obj = session.get_object('org.freedesktop.Notifications', '/org/freedesktop/Notifications')
			notify = dbus.Interface(obj, 'org.freedesktop.Notifications')
			notify.Notify('rootforce', 0, 'dialog-password', 'Raised permissions', 'Raised permissions of {0} [{1}]'.format(get_proc_name(pid), pid), [], {}, 5000)
		except Exception:
			pass
		return True
	else:
		return False
	

def lower_perms():
	if do_lower_perms():
		try:
			session = dbus.SessionBus()
			obj = session.get_object('org.freedesktop.Notifications', '/org/freedesktop/Notifications')
			notify = dbus.Interface(obj, 'org.freedesktop.Notifications')
			notify.Notify('rootforce', 0, 'dialog-information', 'Restored permissions', 'Restored permissions changed by rootforce', [], {}, 4000)
		except Exception:
			pass
		print("Permissions restored.")
		return True
	else:
		return False

mode = get_display_type()
if mode == 'terminal':
	mypid = get_terminal_foreground_pid()
else:
	mywin = get_x_foreground_window()
	mypid = get_x_foreground_pid(mywin)
myname = get_proc_name(mypid)


if len(sys.argv) > 1:
	if sys.argv[1] in ('-d', '--disable', 'disable'):
		if os.path.exists('/sys/module/rootforce/parameters/target_pid'):
			try:
				with open('/sys/module/rootforce/parameters/target_pid', 'r') as f:
					_pid = int(f.readline())
					if _pid != 0:
						lower_perms()
			except Exception:
				lower_perms()
		exit(0)
	elif sys.argv[1].isdigit():
		pid = int(sys.argv[1])
		raise_perms(pid)
	else:
		print('Usage: {0} [-d | pid]'.format(sys.argv[0]))
		exit(1)
else:
	if myname == 'yakuake':
		print ('Found Yakuake, using app specific code to get foreground pid')
		session = dbus.SessionBus()
		obj = session.get_object('org.kde.yakuake', '/yakuake/sessions')
		sessid = obj.activeSessionId()
		obj = session.get_object('org.kde.yakuake', '/Sessions/'+str(sessid+1))
		mypid = obj.processId()
	if myname == 'konsole':
		print ('Found Konsole, using app specific code to get foreground pid')
		session = dbus.SessionBus()
		obj = session.get_object('org.kde.konsole-'+str(mypid), '/Konsole')
		sessid = obj.currentSession()
		obj = session.get_object('org.kde.konsole-'+str(mypid), '/Sessions/'+str(sessid))
		mypid = obj.foregroundProcessId()
	if myname == 'kate':
		try:
			print ('Found Kate, using app specific code to get foreground pid')
			session = dbus.SessionBus()
			obj = session.get_object('org.kde.kate-'+str(mypid), '/Konsole')
			sessid = obj.currentSession()
			if sessid > 0:
				obj = session.get_object('org.kde.kate-'+str(mypid), '/Sessions/'+str(sessid))
				mypid = obj.foregroundProcessId()
		except Exception:
			pass

	selfpid = os.getpid()
	pid = max((get_pid_starttime(pid), pid) for pid in get_descendant_pids(mypid) if pid != selfpid)[1]

	raise_perms(pid)
