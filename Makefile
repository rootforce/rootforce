#!/usr/bin/make -f
# (c) 2012 Christoph Grenz <christophg@grenz-bonn.de>
# This file is part of rootforce.
#
# Rootforce is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# Rootforce is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rootforce.  If not, see <http://www.gnu.org/licenses/>.

STATIC_FILES := do_rootforce undo_rootforce
DYNAMIC_FILES := rootforce de.grenz-bonn.rootforce.policy

PREFIX := /usr/local
BINDIR := $(PREFIX)/bin
SBINDIR := $(PREFIX)/sbin
APPDIR := $(PREFIX)/share/applications
POLKITACTIONDIR := $(PREFIX)/share/polkit-1/actions
DKMS ?= 0

ifeq ($(DKMS),1)
	MODMFLAGS := -f Makefile.dkms
else
	MODMFLAGS :=
endif

manmake = @$(MAKE) -C man
modulemake = @$(MAKE) -C module $(MODMFLAGS)

# Phony targets

all: all-without-module module

all-without-module: $(STATIC_FILES) $(DYNAMIC_FILES) man

clean:
	rm $(DYNAMIC_FILES) || true
	$(modulemake) clean
	$(manmake) clean

install: all
	install -m 755 rootforce "$(BINDIR)"/
	install -m 744 do_rootforce "$(SBINDIR)"/
	install -m 744 undo_rootforce  "$(SBINDIR)"/
	test -z "$(POLKITACTIONDIR)" || install -d "$(POLKITACTIONDIR)"
	test -z "$(POLKITACTIONDIR)" || install -m 644 de.grenz-bonn.rootforce.policy "$(POLKITACTIONDIR)/"
	install -d "$(APPDIR)"
	install -m 644 rootforce.desktop "$(APPDIR)"/
	$(modulemake) install
	$(manmake) install

uninstall:
	rm "$(BINDIR)"/rootforce
	rm "$(SBINDIR)"/do_rootforce
	rm "$(SBINDIR)"/undo_rootforce
	test -z "$(POLKITACTIONDIR)" || rm "$(POLKITACTIONDIR)"/de.grenz-bonn.rootforce.policy
	rm "$(APPDIR)"/rootforce.desktop
	$(modulemake) uninstall
	$(manmake) uninstall

module:
	$(modulemake)

man:
	$(manmake)
	

# Phony targets for release builds
VERSION = $(shell cat debian/changelog | head -n 1 | awk '{ print $$2 }' | tail -c +2 | head -c -2)

release: release-tar release-deb

release-tar: clean
	tar -cjf "../rootforce_$(VERSION).tar.bz2" --xform "s|^./|rootforce/|" --exclude ".git" --owner=nobody --group=nogroup --posix  .

release-deb: clean
	debuild -tc


# Rules

rootforce: rootforce.py
	sed "s|^SBINDIR=.*|SBINDIR='"$(SBINDIR)"'|" rootforce.py > rootforce
	chmod a+x rootforce

%.policy: %.policy.tpl
	sed "s|\$$SBINDIR|$(SBINDIR)|g" $< > $@


.PHONY: all all-without-module clean install uninstall module man release release-tar release-deb
