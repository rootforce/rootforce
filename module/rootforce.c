// (c) 2015 Christoph Grenz <christophg@grenz-bonn.de>
// This file is part of rootforce.
//
// Rootforce is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// Rootforce is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rootforce.  If not, see <http://www.gnu.org/licenses/>.

/* Standard headers for LKMs */
#include <linux/module.h>  
#include <linux/printk.h>      /* console_print() interface */

#include <linux/sched.h>
#include <linux/capability.h>
#include <linux/security.h>
#include <linux/securebits.h>
#include <linux/pid.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Christoph Grenz <christophg@grenz-bonn.de");
MODULE_DESCRIPTION("Raises the capabilities of a process on-the-fly.\n  Target PID can be changed on runtime through /sys/modules/rootforce/parameters/target_pid");

static bool initialized = false;

static int param_set_pid(const char *val, const struct kernel_param *kp);
static const struct kernel_param_ops param_ops_target_pid = { .set = &param_set_pid, .get = &param_get_int };

static int target_pid = 0;
module_param_cb(target_pid, &param_ops_target_pid, &target_pid, 0644);
MODULE_PARM_DESC(target_pid, " Process whose capabilities should be raised (int)");

static bool change_euid = false;
module_param(change_euid, bool, 0644);
MODULE_PARM_DESC(change_euid, " Whether the effective user id should be changed to 0");

static int caps_inheritable = 0;
module_param(caps_inheritable, int, 0644);
MODULE_PARM_DESC(caps_inheritable, " 1: capabilities can be inherited using capset() 2: all capabilities will be inherited on fork() and exec()");

static struct target_t {
	int pid_nr;
	struct pid *pid;
	const struct cred *old_cred;
	struct cred *mod_cred;
} target = {
	pid_nr:	0,
	pid:	NULL,
	old_cred:	NULL,
	mod_cred:	NULL
};

static void init_target(struct target_t *target, int pid) {
	target->pid_nr = pid;
	target->pid = find_vpid(pid);
	get_pid(target->pid);
	target->old_cred = NULL;
	target->mod_cred = NULL;
}

static void destroy_target(struct target_t *target) {
	target->pid_nr = 0;
	if (target->pid) put_pid(target->pid);
	target->pid = NULL;
	target->old_cred = NULL;
	target->mod_cred = NULL;
}

static void restore_pid_perms(struct target_t *target) {
	struct task_struct *task;
	struct pid *pid = target->pid;
	
	if (pid) {
		task = get_pid_task(pid, PIDTYPE_PID);
		if (task) {
			if (task->cred == target->mod_cred) {
				task->cred = target->old_cred;
				task->real_cred = task->cred;
				put_cred(target->mod_cred);
				put_cred(target->mod_cred);
				printk(KERN_INFO "rootforce: Reverted creds for %i\n", target->pid_nr);
			}
			else {
				// If task creds changed old_cred should have two references too much
				// and mod_cred should have lost two.
				// So put old_cred here twice and mod_cred only implicit in abort_creds
				put_cred(target->old_cred);
				put_cred(target->old_cred);
				printk(KERN_INFO "rootforce: Could not revert creds for %i\n", target->pid_nr);
			}
			put_task_struct(task);
		}
		else {
			put_cred(target->old_cred);
			put_cred(target->old_cred);
			printk(KERN_INFO "rootforce: Could not revert creds as process %i is already dead.\n", target->pid_nr);
		}
		abort_creds(target->mod_cred);
		put_cred(target->old_cred);
		target->mod_cred = NULL;
		target->old_cred = NULL;
	}
}

static void modforce_pid_perms(struct target_t *target) {
	struct task_struct *task;
	kernel_cap_t newcaps;
	
	if (target->pid) {
		// Restore original creds if already modified to get to a defined state
		if (target->mod_cred) restore_pid_perms(target);
		
		task = get_pid_task(target->pid, PIDTYPE_PID);
		if (task) {
			if (task->cred == task->real_cred) {
				get_cred(task->cred);
				target->mod_cred = prepare_kernel_cred(task);
				get_cred(target->mod_cred);
				get_cred(target->mod_cred);
				target->old_cred = task->cred;
				
				newcaps = CAP_FULL_SET;
				cap_lower(newcaps, CAP_SETPCAP);
				
				// Always modify effective set
				target->mod_cred->cap_effective = newcaps;
				// If requested modify permitted set
				if (caps_inheritable >= 1)
					target->mod_cred->cap_permitted = newcaps;
				// If requested also modify inheritable set
				if (caps_inheritable >= 2)
					target->mod_cred->cap_inheritable = newcaps;
				
				// If requested change EUID to 0
				if (change_euid) {
					target->mod_cred->euid.val = 0;
					// Set NOROOT securebit and lock it when no inheritance is allowed
					if (caps_inheritable < 1)
						 target->mod_cred->securebits |= 1<<SECBIT_NOROOT | 1<<SECBIT_NOROOT_LOCKED;
					// Set NOROOT securebit if only explicit inheritance is allowed
					else if (caps_inheritable < 2)
						target->mod_cred->securebits |= 1<<SECBIT_NOROOT;
				}
				
				task->cred = target->mod_cred;
				task->real_cred = task->cred;
				printk(KERN_INFO "rootforce: Changed creds for %i\n", target->pid_nr);
			}
		}
		put_task_struct(task);
	}
}

static int param_set_pid(const char * val, const struct kernel_param *kp) {
	int result;
	result = param_set_int(val, kp);
	
	if (initialized) {
		restore_pid_perms(&target);
		destroy_target(&target);
		if (target_pid > 0) {
			init_target(&target, target_pid);
			modforce_pid_perms(&target);
		}
	}
	return result;
}


/* Initialize the LKM */
int __init init_module()
{
	printk(KERN_DEBUG "rootforce loaded\n");
	
	initialized = true;
	
	if (target_pid) {
		init_target(&target, target_pid);
		modforce_pid_perms(&target);
	}
	
	return 0;
}


/* Cleanup - undo whatever init_module did */
void __exit cleanup_module()
{
	restore_pid_perms(&target);
	destroy_target(&target);
	printk(KERN_DEBUG "rootforce unloaded\n");
}
